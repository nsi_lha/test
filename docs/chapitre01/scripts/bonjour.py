# --- PYODIDE:code --- #

def accueil(prenom):
    ...


# --- PYODIDE:corr --- #

def accueil(prenom):
    return "Bonjour " + prenom


# --- PYODIDE:tests --- #

assert accueil("Alice") == "Bonjour Alice"
assert accueil("Bob")  == "Bonjour Bob"


# --- PYODIDE:secrets --- #

assert accueil("fegrehjtyjtqfqsgeryryrfg") == "Bonjour fegrehjtyjtqfqsgeryryrfg"