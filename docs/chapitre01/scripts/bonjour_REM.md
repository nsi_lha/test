!!! info "Expression booléenne"

    Remarquons que `nombre % 2 == 0` est une expression booléenne qui s'évalue à `True` ou `False`

Il est **très maladroit** d'écrire

```python
def est_pair(nombre):
    if nombre % 2 == 0:
        return True
    else:
        return False
```